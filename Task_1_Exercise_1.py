import argparse
import operator

parser = argparse.ArgumentParser(description='')
parser.add_argument('left_operand',type=float)
parser.add_argument('operator')
parser.add_argument('right_operand',type=float)
args = parser.parse_args()


operators = {'*':operator.mul, '+':operator.add, '-':operator.sub, '/':operator.truediv}
if args.operator in operators.keys():
    print(operators[args.operator](args.left_operand , args.right_operand))

else:
    raise NotImplementedError()